FROM registry.cirici.com/alvarium/infrastructure/php-docker:apache

COPY . /var/www
WORKDIR /var/www

RUN apt-get update && apt-get install -y cron

EXPOSE 80

CMD ["/var/www/entrypoint.sh"]
